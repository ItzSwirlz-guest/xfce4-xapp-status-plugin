# Chinese (Hong Kong) translation for linuxmint
# Copyright (c) 2020 Rosetta Contributors and Canonical Ltd 2020
# This file is distributed under the same license as the linuxmint package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: linuxmint\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2019-10-14 16:48+0100\n"
"PO-Revision-Date: 2020-06-24 19:51+0000\n"
"Last-Translator: Robert K <Unknown>\n"
"Language-Team: Chinese (Hong Kong) <zh_HK@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-12-10 10:32+0000\n"
"X-Generator: Launchpad (build 4853cb86c14c5a9e513816c8a61121c639b30835)\n"

#: plugin/xapp-status-plugin.c:204 plugin/xapp-status-plugin.desktop.in:4
msgid "XApp Status Plugin"
msgstr "XApp狀態插件"

#: plugin/xapp-status-plugin.c:209 plugin/xapp-status-plugin.desktop.in:5
msgid "Area where XApp Status icons appear"
msgstr "XApp狀態圖標出現的區域"

#: plugin/xapp-status-plugin.c:220
msgid "About"
msgstr "關於"
